﻿using Raidbot.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowDotNetAPI;
using WowDotNetAPI.Models;

namespace Raidbot.Models
{
    [Serializable]
    public class RaidMember
    {
        public string Name { get; set; }
        public string Realm { get; set; }
        public int EqItemLevel { get; set; }
        public string Spec { get; set; }
        public CharacterClass CharacterClass { get; set; }
        public ulong DiscordId { get; set; }

        public RaidMember(Character character, string spec, ulong discordId)
        {
            Name = character.Name;
            Realm = character.Realm;
            EqItemLevel = character.Items.AverageItemLevelEquipped;
            Spec = spec.ToLower();
            CharacterClass = character.Class;
            DiscordId = discordId;
        }

        public RaidMember()
        {

        }
    }
}
