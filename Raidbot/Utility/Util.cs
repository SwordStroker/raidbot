﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raidbot.Utility
{
    public static class Util
    {
        public static bool CheckRaidLeaderRole(ICommandContext context,IReadOnlyCollection<ulong> userRoleIds)
        {
            ulong raidLeaderId = 0;
            foreach (var item in context.Guild.Roles)
                if (item.Name == Const.RAIDLEADER_ROLE)
                    raidLeaderId = item.Id;

            return userRoleIds.Contains(raidLeaderId);
        }
    }
}
