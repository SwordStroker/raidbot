﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using System.IO;
using Raidbot.Handlers;

namespace Raidbot
{
    public class Program
    {
        private CommandService commands;
        private DiscordSocketClient client;
        private IServiceProvider services;

        private string tokenTextPathString = Directory.GetCurrentDirectory() + "\\DiscordToken.txt";

        static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();


        public async Task MainAsync()
        {
            client = new DiscordSocketClient();
            commands = new CommandService();

            string token = File.ReadAllLines(tokenTextPathString)[0];

            WowHandler.Setup();
            DirectoryHandler.DirectoryCheck();

            client.Log += Log;
            client.Ready += () =>
            {
                Console.WriteLine("Bot is Connected!");
                return Task.FromResult(0);
            };         

            services = new ServiceCollection().BuildServiceProvider();

            await InstallCommands();

            await client.LoginAsync(TokenType.Bot, token);
            await client.StartAsync();

            // Block this task until the program is closed.
            await Task.Delay(-1);
        }

        public async Task InstallCommands()
        {
            client.MessageReceived += HandleCommand;

            await commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        public async Task HandleCommand(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            if (message == null) return;
            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;
            // Determine if the message is a command, based on if it starts with '!' or a mention prefix
            if (!(message.HasCharPrefix('!', ref argPos) || message.HasMentionPrefix(client.CurrentUser, ref argPos))) return;
            // Create a Command Context
            if (message.Content == "!postchangelog")
            {
                var context2 = new CommandContext(client, message);
                var result2 = await commands.ExecuteAsync(context2, argPos, services);
                if(!result2.IsSuccess)
                    await context2.Channel.SendMessageAsync(result2.ErrorReason + "\nPlease use !help to list the commands.");
                return;
            }
            if (message.Channel.Name != "raidbot" && !RaidHandler.CheckRaid(message.Channel.Name)) return;
            var context = new CommandContext(client, message);
            // Execute the command. (result does not indicate a return value, 
            // rather an object stating if the command executed successfully)
            var result = await commands.ExecuteAsync(context, argPos, services);
            if (!result.IsSuccess)
                await context.Channel.SendMessageAsync(result.ErrorReason + "\nPlease use !help to list the commands.");
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.FromResult(0);
        }
    }
}
