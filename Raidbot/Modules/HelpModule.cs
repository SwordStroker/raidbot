﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raidbot.Modules
{
    public class HelpModule : ModuleBase
    {
        private CommandService _service;
        public HelpModule(CommandService service)
        {
            _service = service;
        }

        [Command("help"),Summary("Will list you the commands you can use.")]
        public async Task HelpAsync()
        {
            string prefix = "!";
            var builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Description = "These are the commands you can use"
            };

            foreach (var module in _service.Modules)
            {
                if (module.Name == "BotModule") continue;
                foreach (var cmd in module.Commands)
                {
                    var result = await cmd.CheckPreconditionsAsync(Context);
                    if (result.IsSuccess)
                    {
                        builder.AddField(x =>
                        {
                            x.Name = $"{prefix}{cmd.Aliases.First()}";
                            x.Value = cmd.Summary;
                        });
                    }
                }
            }

            await Context.User.SendMessageAsync("", false, builder.Build());
        }
    }
}
