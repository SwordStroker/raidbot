﻿using Discord;
using Discord.Commands;
using Raidbot.Handlers;
using Raidbot.Models;
using Raidbot.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raidbot.Modules
{
    [Group("raid")]
    public class RaidModule : ModuleBase
    {
        [Command("all"), Summary("Shows all of the registered raids.\nUsage : !raid all")]
        public async Task Raids()
        {
            await ReplyAsync("", false, RaidHandler.ShowRaids());
        }

        [Command("add"), Summary("This command will help you to register a raid.")]
        public async Task AddRaid(string raidName, string raidDate, string raidTime, string characterName, string realm, string spec, int minimumEqItemLevel, [Remainder]string description)
        {
            IGuildUser user = Context.User as IGuildUser;
            if (!Util.CheckRaidLeaderRole(Context, user.RoleIds))
            {
                await ReplyAsync(RaidHandler.PrintMessage("You dont have the permissions"));
                return;
            }
            raidName = raidName.Replace(" ", "-");
            int result = RaidHandler.AddRaid(raidName, raidDate, raidTime, characterName, realm, spec, minimumEqItemLevel, description, Context.User.Id);
            if (result == -1)
                await ReplyAsync(RaidHandler.PrintMessage(string.Format("There is a raid already named {0}", raidName)));
            else if (result == -2)
                await ReplyAsync(RaidHandler.PrintMessage("Please specify your role correctly.{Tank,MDps,RDps,Healer}"));
            else if(result == -3)
                await ReplyAsync(RaidHandler.PrintMessage($"Couldnt find a player named {characterName} in {realm}."));
            else
            {
                await ReplyAsync(string.Format("`Raid {0} has been created successfully by {1}`", raidName, Context.User.Username));
                IRole role = await Context.Guild.CreateRoleAsync(raidName.ToLower());
                await ((IGuildUser)Context.User).AddRoleAsync(role);
                IMessageChannel newc = await Context.Guild.CreateTextChannelAsync(raidName);
                await newc.SendMessageAsync(RaidHandler.PrintMessage(description));
            }
        }

        [Command("details"), Summary("This command will give you the details of a raid.\nUsage: !raid details raidName(raidName is optional,if you dont specify the raid name,it'll use channel's name)")]
        public async Task Detail(string raidName = null)
        {
            string name = string.IsNullOrEmpty(raidName) ? Context.Channel.Name : raidName;
            if (!RaidHandler.CheckRaid(name))
            {
                await ReplyAsync($"No raid named {name} found..");
                return;
            }
            EmbedBuilder builder = RaidHandler.ShowRaid(name);
            await ReplyAsync("", false, builder);
        }

        [Command("register"), Summary("This command will register you to a raid.You have to use this command on a registered raid's channel.\nParameters: !raid register <CharacterName> <Realm> <Role> \nUsage: !raid register Cthraxxi Silvermoon mdps\n\n`Important Note:If you have space between your realm name like Scarshield Legion,you have to type it in quotes like this \"ScarShield Legion\"`")]
        public async Task SignUp(string characterName, string realm, string spec)
        {
            int result = RaidHandler.AddRaidMember(Context.Channel.Name, characterName, realm, spec, Context.User.Id);
            if (result == -1)
                await ReplyAsync(RaidHandler.PrintMessage("No raid named " + Context.Channel.Name + " found!"));
            else if (result == -2)
                await ReplyAsync(RaidHandler.PrintMessage("You are already added to this raid."));
            else if (result == -3)
                await ReplyAsync(RaidHandler.PrintMessage("Please specify your role correctly.{Tank,MDps,RDps,Healer}"));
            else if (result == -4)
                await ReplyAsync(RaidHandler.PrintMessage($"Couldnt find a player named {characterName} in {realm}."));
            else if (result == -5)
                await ReplyAsync(RaidHandler.PrintMessage("Your item eq level is lower than raid's minimum"));
            else
            {
                await ReplyAsync(RaidHandler.PrintMessage(characterName + " is successfully added to " + Context.Channel.Name));
                IRole role = Context.Guild.Roles.FirstOrDefault(r => r.Name == Context.Channel.Name.ToLower());
                await ((IGuildUser)Context.User).AddRoleAsync(role);
            }
            //await ReplyAsync(RaidHandler.AddRaidMember(Context.Channel.Name, characterName, realm, spec, Context.User.Id));
        }

        [Command("cancel"), Summary("This command will cancel the raid.\nUsage: !raid cancel raidName (raidName is optional,if you dont specify the raid name,it'll use channel's name)")]
        public async Task Cancel(string raidName = null)
        {
            string name = string.IsNullOrEmpty(raidName) ? Context.Channel.Name : raidName;
            int result = RaidHandler.CancelRaid(name, Context.User.Id, Context.User.Username, Context);
            IGuildUser user = Context.User as IGuildUser;
            if (result == -1)
                await ReplyAsync(RaidHandler.PrintMessage("No raid named " + name + " found!"));
            else if (!Util.CheckRaidLeaderRole(Context, user.RoleIds))
            {
                await ReplyAsync(RaidHandler.PrintMessage("You dont have the permissions"));
                return;
            }
            else
            {
                IRole role = Context.Guild.Roles.FirstOrDefault(r => r.Name == name);
                var channels = await Context.Guild.GetChannelsAsync();
                await ((IMessageChannel)channels.FirstOrDefault(c => c.Name == name)).SendMessageAsync(role.Mention + " raid has been cancelled");
                //await ReplyAsync(role.Mention + " raid has been cancelled");
                await role.DeleteAsync();
                RaidHandler.RemoveRaid(name);

                //var users = await Context.Guild.GetUsersAsync(CacheMode.AllowDownload);
                //Raid raid = RaidHandler.GetRaid(name);
                //string mentionString = "";
                //foreach (var item in raid.MemberList)
                //{
                //    IGuildUser user = users.SingleOrDefault(i => i.Id == item.DiscordId);
                //    if (user != null)
                //    {
                //        mentionString += user.Mention + ",";
                //        //await user.SendMessageAsync($"{name} raid has been cancelled");
                //    }
                //}
                //await ReplyAsync(mentionString.TrimEnd(',') + " raid has been cancelled");
                //also delete the channel that is created for that raid.
            }
        }

        [Command("sendinvites"), Summary("This command will send a quick message to raid channel,notify the users thats registered to the raid.\n Usage: !raid sendinvites")]
        public async Task SendInvites(string raidName = null)
        {
            string name = string.IsNullOrEmpty(raidName) ? Context.Channel.Name : raidName;

            IGuildUser user = Context.User as IGuildUser;
            if (!Util.CheckRaidLeaderRole(Context, user.RoleIds))
            {
                await ReplyAsync(RaidHandler.PrintMessage("You dont have the permissions"));
                return;
            }

            if (!RaidHandler.CheckRaid(name))
            {
                await ReplyAsync($"No raid named {name} found");
                return;
            }

            IRole role = Context.Guild.Roles.FirstOrDefault(r => r.Name == name);
            var channels = await Context.Guild.GetChannelsAsync();
            await ((IMessageChannel)channels.FirstOrDefault(c => c.Name == name)).SendMessageAsync(role.Mention + " invites will start shortly.");
        }

        [Command("cancelregister"), Summary("This command will cancel your registration to the raid.You have to use this command on a registered raid's channel.\nUsage: !raid cancelregister raidName (raidName is optional,if you dont specify the raid name,it'll use channel's name)")]
        public async Task CancelSignUp(string raidName = null)
        {
            string name = string.IsNullOrEmpty(raidName) ? Context.Channel.Name : raidName;
            await ReplyAsync(RaidHandler.DeleteRaidMember(name, Context.User.Id));
        }

        [Command("end"), Summary("This command will end the raid.\nUsage: !raid finishraid raidName (raidName is optional,if you dont specify the raid name,it'll use channel's name)")]
        public async Task EndRaid(string raidName = null)
        {
            string name = string.IsNullOrEmpty(raidName) ? Context.Channel.Name : raidName;
            bool raidAvailability = RaidHandler.CheckRaid(name);
            if (!raidAvailability)
                await ReplyAsync(RaidHandler.PrintMessage($"No raid named {name} found"));
            else
            {
                IRole role = Context.Guild.Roles.FirstOrDefault(r => r.Name == name);
                var channels = await Context.Guild.GetChannelsAsync();
                await ((IMessageChannel)channels.FirstOrDefault(c => c.Name == name)).SendMessageAsync(role.Mention + " raid has been finished.Thanks for joining!");
                await role.DeleteAsync();
                RaidHandler.RemoveRaid(name);
            }
        }
    }
}
