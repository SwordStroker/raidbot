﻿using Newtonsoft.Json;
using Raidbot.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raidbot.Handlers
{
    public static class DirectoryHandler
    {
        public static string raidFolderPathString = Directory.GetCurrentDirectory() + "\\Raids";
        public static string raidTextPathString = raidFolderPathString + "\\Raids.txt";
        public static string changeLogTextPathString = Directory.GetCurrentDirectory() + "\\ChangeLog.txt";

        public static void DirectoryCheck()
        {
            if (!Directory.Exists(raidFolderPathString))
            {
                Directory.CreateDirectory(raidFolderPathString);
                File.CreateText(raidTextPathString);
                File.CreateText(changeLogTextPathString);
                RaidHandler.raidList = new List<Raid>();
            }
            else
            {
                string[] files = Directory.GetFiles(raidFolderPathString);
                string raids = File.ReadAllText(raidTextPathString);
                if (!string.IsNullOrEmpty(raids))
                    RaidHandler.raidList = JsonConvert.DeserializeObject<List<Raid>>(raids);
                else
                    RaidHandler.raidList = new List<Raid>();
            }
        }

        public static void SaveRaids()
        {
            File.WriteAllText(raidTextPathString, JsonConvert.SerializeObject(RaidHandler.raidList));
        }
    }
}
