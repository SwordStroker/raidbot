﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raidbot.Models;
using Raidbot.Utility;
using WowDotNetAPI.Models;
using Raid = Raidbot.Models.Raid;
using Discord;
using Discord.Commands;

namespace Raidbot.Handlers
{
    public abstract class RaidHandler
    {
        public static List<Raid> raidList;

        public static int AddRaid(string raidName, string raidDate, string raidTime, string name, string realm, string spec, int minimumEqItemLevel, string description, ulong discordId)
        {
            if (CheckRaid(raidName)) return -1;
            if (!Const.Specs.Contains(spec.ToLower())) return -2;
            Character character = WowHandler.GetCharacter(name, realm, WowDotNetAPI.CharacterOptions.GetItems | WowDotNetAPI.CharacterOptions.GetTalents);
            if (character == null)
                return -3;
            Raid raid = new Raid()
            {
                Name = raidName.Trim(),
                Description = description,
                Date = raidDate,
                Time = raidTime,
                MinimumEqItemLevel = minimumEqItemLevel,
                CreatorDiscordId = discordId,
                MemberList = new List<RaidMember>() { new RaidMember(character, spec, discordId) }
            };

            raidList.Add(raid);
            SaveRaids();
            return 0;
        }

        public static int AddRaidMember(string raidName, string characterName, string realm, string spec, ulong discordId)
        {
            Raid raid = raidList.Find(r => string.Equals(r.Name, raidName, StringComparison.OrdinalIgnoreCase));
            Character character = null;
            if (raid == null)
                return -1;
            //return PrintMessage("No raid named " + raidName + " found!");
            RaidMember raidMember = raid.MemberList.Find(r => r.Name == CapitalizeFirstLetter(characterName));
            if (raidMember != null)
                return -2;
            //return PrintMessage("You are already added to this raid.");
            if (!Const.Specs.Contains(spec.ToLower()))
                return -3;
            //return PrintMessage("Please specify your role correctly.{Tank,MDps,RDps,Healer}");
            try
            {
                character = WowHandler.GetCharacter(characterName, realm, WowDotNetAPI.CharacterOptions.GetTalents | WowDotNetAPI.CharacterOptions.GetItems);
            }
            catch (Exception ex)
            {
                return -4;
            }
            if (character.Items.AverageItemLevelEquipped < raid.MinimumEqItemLevel)
                return -5;
            //return PrintMessage(string.Format("Your item eq level is {0}, raid's minimum is {1}..", character.Items.AverageItemLevelEquipped, raid.MinimumEqItemLevel));
            raid.MemberList.Add(new RaidMember() { Name = character.Name, CharacterClass = character.Class, Realm = character.Realm, EqItemLevel = character.Items.AverageItemLevelEquipped, Spec = spec.ToLower(), DiscordId = discordId });
            SaveRaids();
            return 0;
            //return PrintMessage(characterName + " is successfully added to " + raid.Name);
        }

        public static string DeleteRaidMember(string raidName, ulong discordId)
        {
            Raid raid = raidList.Find(r => string.Equals(r.Name, raidName, StringComparison.OrdinalIgnoreCase));
            if (raid == null)
                return PrintMessage("No raid named " + raidName + " found!");
            RaidMember raidMember = raid.MemberList.Find(r => r.DiscordId == discordId);
            if (raidMember == null)
                return PrintMessage($"No Records Found");

            raid.MemberList.Remove(raidMember);
            SaveRaids();
            return PrintMessage(raidMember.Name + " is successfully removed from " + raid.Name + " raid");
        }

        //public static string ShowRaid(string raidName)
        //{
        //    Raid raid = raidList.Find(f => f.Name == raidName);
        //    if (raid == null) return "No raid named " + raidName + " found";

        //    string[] columns = { "Name", "Realm", "ILvl", "Spec", "Class" };
        //    List<Tuple<string, string, int, string, string>> val = new List<Tuple<string, string, int, string, string>>();

        //    foreach (var raidMember in raid.MemberList)
        //    {
        //        val.Add(new Tuple<string, string, int, string, string>(raidMember.Name, raidMember.Realm, raidMember.EqItemLevel, raidMember.Spec, raidMember.CharacterClass.ToString()));
        //    }

        //    return val.ToStringTable(columns, a => a.Item1, a => a.Item2, a => a.Item3, a => a.Item4, a => a.Item5);
        //}

        //public static string ShowAllRaids()
        //{
        //    List<Tuple<string, string, string, int, string, string, string, Tuple<string>>> val = new List<Tuple<string, string, string, int, string, string, string, Tuple<string>>>();

        //    string[] columns = { "RaidName", "Date", "Time", "MinEqILvl", "Tank", "MDps", "RDps", "Healer" };
        //    foreach (var raid in raidList)
        //    {
        //        val.Add(new Tuple<string, string, string, int, string, string, string, Tuple<string>>(raid.Name, raid.Date, raid.Time, raid.MinimumEqItemLevel, raid.GetTankCount(), raid.GetMDpsCount(), raid.GetRDpsCount(), new Tuple<string>(raid.GetHealerCount())));
        //    }
        //    return "`" + val.ToStringTable(columns, a => a.Item1, a => a.Item2, a => a.Item3, a => a.Item4, a => a.Item5, a => a.Item6, a => a.Item7, a => a.Rest.Item1) + "`";
        //}

        public static EmbedBuilder ShowRaid(string raidName)
        {
            Raid raid = GetRaid(raidName);
            if (raid == null) return null;
            EmbedBuilder builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Author = new EmbedAuthorBuilder() { Name = raid.Name },
                Description = raid.Description
            };

            builder
                .AddField("Date", "a", true)
                .AddField("Time", "a", true)
                .AddField("MinEqILvl", "a", true)
                .AddField("Name", "a", true)
                .AddField("ILvl", "a", true)
                .AddField("Class", "a", true)
                .WithFooter($"Current Composition is {raid.GetTankCount()}/{raid.GetHealerCount()}/{raid.GetAllDpsCount()}");

            string memberNames = "";
            string memberilvls = "";
            string memberspecs = "";
            foreach (var item in raid.MemberList)
            {
                memberNames += item.Name + "-" + item.Realm + "\n";
                memberilvls += item.EqItemLevel + "\n";
                memberspecs += item.CharacterClass + "-" + item.Spec + "\n";
            }

            builder.Fields[0].Value = raid.Date;
            builder.Fields[1].Value = raid.Time;
            builder.Fields[2].Value = raid.MinimumEqItemLevel;
            builder.Fields[3].Value = memberNames;
            builder.Fields[4].Value = memberilvls;
            builder.Fields[5].Value = memberspecs;
            return builder;
        }

        public static EmbedBuilder ShowRaids()
        {
            EmbedBuilder builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Author = new EmbedAuthorBuilder() { Name = "All Raids" },
            };

            if (raidList.Count <= 0)
                builder.AddField(x =>
                {
                    x.Name = "Error";
                    x.Value = "No Raids Registered";
                });
            else
            {
                builder.AddField(x =>
                {
                    x.Name = "Raid Name/MinILvl";
                    x.IsInline = true;
                }).AddField(x =>
                {
                    x.Name = "Date/Time";
                    x.IsInline = true;
                }).AddField(x =>
                {
                    x.Name = "Members";
                    x.IsInline = true;
                });

                /*
                 .AddField(x =>
                {
                    x.Name = "MinEqILvl";
                    x.IsInline = true;
                }).AddField(x =>
                {
                    x.Name = "Tank";
                    x.IsInline = true;
                }).AddField(x =>
                {
                    x.Name = "MDps";
                    x.IsInline = true;
                }).AddField(x =>
                {
                    x.Name = "RDps";
                    x.IsInline = true;
                }).AddField(x =>
                {
                    x.Name = "Healer";
                    x.IsInline = true;
                }) */
                string raidNames = "";
                string dates = "";
                string members = "";
                //string minEqILvls = "";
                //string tanks = "";
                //string mdpss = "";
                //string rdpss = "";
                //string healers = "";

                foreach (var raid in raidList)
                {
                    raidNames += raid.Name + " /" + raid.MinimumEqItemLevel.ToString() + "\n\n";
                    dates += raid.Date + "/" + raid.Time + "\n\n";
                    members += raid.MemberList.Count.ToString() + "\n\n";
                    //minEqILvls += raid.MinimumEqItemLevel + "\n\n";
                    //tanks += raid.GetTankCount() + "\n\n";
                    //mdpss += raid.GetMDpsCount() + "\n\n";
                    //rdpss += raid.GetRDpsCount() + "\n\n";
                    //healers += raid.GetHealerCount() + "\n\n";
                }
                builder.Fields[0].Value = raidNames.TrimEnd();
                builder.Fields[1].Value = dates.TrimEnd();
                builder.Fields[2].Value = members.TrimEnd();
                //builder.Fields[3].Value = minEqILvls.TrimEnd();
                //builder.Fields[4].Value = tanks.TrimEnd();
                //builder.Fields[5].Value = mdpss.TrimEnd();
                //builder.Fields[6].Value = rdpss.TrimEnd();
                //builder.Fields[7].Value = healers.TrimEnd();
            }
            return builder;
        }

        private static void SaveRaids()
        {
            DirectoryHandler.SaveRaids();
        }

        public static string PrintMessage(string message)
        {
            return "`" + message + "`";
        }

        private static string CapitalizeFirstLetter(string s)
        {
            if (string.IsNullOrEmpty(s))
                return s;
            if (s.Length == 1)
                return s.ToUpper();
            return s.Remove(1).ToUpper() + s.Substring(1);
        }

        public static string ChangeDateOfRaid(string raidName, string newDate, ulong discordId)
        {
            Raid raid = raidList.Find(r => r.Name == raidName);
            string oldDate = raid.Date;
            if (raid == null)
                return PrintMessage("No raid named " + raidName + " found!");

            if (raid.CreatorDiscordId != discordId)
                return PrintMessage("You are not allowed to change the date.");

            raid.Date = newDate;
            SaveRaids();
            return PrintMessage(string.Format("{0} raid date is changed to {0} from {1}", newDate, oldDate));
        }

        public static string ChangeTimeOfRaid(string raidName, string newTime, ulong discordId)
        {
            Raid raid = raidList.Find(r => r.Name == raidName);
            string oldTime = raid.Time;
            if (raid == null)
                return PrintMessage("No raid named " + raidName + " found!");

            if (raid.CreatorDiscordId != discordId)
                return PrintMessage("You are not allowed to change the date.");

            raid.Date = newTime;
            SaveRaids();
            return PrintMessage(string.Format("{0} raid date is changed to {0} from {1}", newTime, oldTime));
        }

        public static string ChangeMinimumILvlOfRaid(string raidName, int newILvl, ulong discordId)
        {
            Raid raid = raidList.Find(r => r.Name == raidName);
            int oldILvl = raid.MinimumEqItemLevel;
            if (raid == null)
                return PrintMessage("No raid named " + raidName + " found!");

            if (raid.CreatorDiscordId != discordId)
                return PrintMessage("You are not allowed to change the eq ilvl.");

            raid.MinimumEqItemLevel = newILvl;
            SaveRaids();
            return PrintMessage(string.Format("{0} raid Minimum Eq ILvl is changed to {0} from {1}", newILvl, oldILvl));
        }

        internal static bool CheckRaid(string raidName)
        {
            Raid raid = raidList.Find(r => string.Equals(r.Name, raidName, StringComparison.OrdinalIgnoreCase));
            return raid != null;
        }

        internal static int CheckRaidAndPermission(string raidName, ulong discordId)
        {
            Raid raid = raidList.Find(r => r.Name == raidName);
            if (raid == null) return -1;
            if (raid.CreatorDiscordId != discordId) return -2;
            return 0;
        }

        public static string CheckGuldanAchi(string characterName, string realm)
        {
            return WowHandler.CheckGuldanCurveAchievement(characterName, realm) ? "He has it" : "He doesnt";
        }

        public static int CancelRaid(string raidName, ulong discordId, string userName, ICommandContext context)
        {
            Raid raid = raidList.Find(r => string.Equals(r.Name, raidName, StringComparison.OrdinalIgnoreCase));
            if (raid == null)
                return -1;
            //return PrintMessage("No raid named " + raidName + " found!");
            if (raid.CreatorDiscordId != discordId)
                return -2;
            //return PrintMessage("You are not allowed to cancel the raid.");

            //raidList.Remove(raid);
            //SaveRaids();
            //PrintMessage(string.Format("{0} has been succesfully canceled by {1}", raidName, userName))
            return 0;
        }

        public static void RemoveRaid(string raidName)
        {
            Raid raid = raidList.Find(r => string.Equals(r.Name, raidName, StringComparison.OrdinalIgnoreCase));
            if (raid != null)
            {
                raidList.Remove(raid);
                SaveRaids();
            }
        }

        public static Raid GetRaid(string raidName) => raidList.Find(r => string.Equals(r.Name, raidName, StringComparison.OrdinalIgnoreCase));

        public static string ShowMyRaids(string characterName, string realm)
        {
            string[] columns = { "RaidName", "Date", "Time", "Spec" };

            List<Tuple<string, string, string, string>> val = new List<Tuple<string, string, string, string>>();
            foreach (var raid in raidList)
            {
                RaidMember member = raid.MemberList.Find(m => m.Name == characterName && m.Realm == realm);
                if (member != null)
                    val.Add(new Tuple<string, string, string, string>(raid.Name, raid.Date, raid.Time, member.Spec));
            }
            return val.Count <= 0 ? "You are not signed to any raid" : val.ToStringTable(columns, a => a.Item1, a => a.Item2, a => a.Item3, a => a.Item4);
        }

    }
}
