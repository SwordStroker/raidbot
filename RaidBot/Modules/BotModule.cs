﻿using Discord;
using Discord.Commands;
using Raidbot.Handlers;
using Raidbot.Utility;
using RaidBot.Handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaidBot.Modules
{
    public class BotModule : ModuleBase
    {
        [Command("postchangelog")]
        public async Task PostChangeLog()
        {
            IReadOnlyCollection<IGuild> guilds = await Context.Client.GetGuildsAsync(CacheMode.AllowDownload);
            foreach (var guild in guilds)
            {
                IReadOnlyCollection<ITextChannel> channelList = await guild.GetTextChannelsAsync(CacheMode.AllowDownload);
                foreach (var channel in channelList)
                {
                    if (channel.Name == Const.CHANGELOG_CHANNEL)
                    {
                        await channel.SendMessageAsync("", false, BotHandler.PostChangeLog());
                    }
                }
            }
        }
    }
}
