﻿using Discord;
using Raidbot.Handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaidBot.Handlers
{
    public static class BotHandler
    {
        public static EmbedBuilder PostChangeLog()
        {
            EmbedBuilder builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Author = new EmbedAuthorBuilder() { Name = "Change Log" }
            };
            string[] changeLogArray = File.ReadAllLines(DirectoryHandler.changeLogTextPathString);
            builder.AddField("Changes", "a", true);
            string changeLogString = "";
            for (int i = 0; i < changeLogArray.Length; i++)
            {
                changeLogString += changeLogArray[i] + "\n";
            }
            builder.Fields[0].Value = changeLogString.TrimEnd(new char[] { '\\', 'n' });
            return builder;
        }
    }
}
